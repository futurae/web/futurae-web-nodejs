var crypto = require('crypto');

var INIT_EXPIRY = 600;
var FUTURAE_EXPIRY = 600;
var APP_EXPIRY = 3600;

var WID_LEN = 36;
var WKEY_LEN = 40;
var SKEY_LEN = 40;

var REQUEST_PREFIX = 'REQ';
var APP_PREFIX = 'APP';
var AUTH_PREFIX = 'AUTH';

/* Exception Messages */
var ERR_WID = 'ERR|The Futurae Web ID in the sign_request() call is invalid.';
var ERR_WKEY = 'ERR|The Futurae Web Key in the sign_request() call is invalid.';
var ERR_SKEY = 'ERR|The Secret Key in the sign_request() call must be at least ' + String(SKEY_LEN) + 'characters.';
var ERR_USERNAME = 'ERR|The username in the sign_request() call is invalid.';

class UsernameError extends Error {
  constructor (message) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

class WidError extends Error {
  constructor (message) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

class SkeyError extends Error {
  constructor (message) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

/**
 * @function sign a value
 *
 * @param {String} key Secret Key
 * @param {String} vals Values to sign
 * @param {String} prefix REQ|APP|AUTH Prefix
 * @param {Integer} expiry expiration time
 *
 * @return {String} Containing the signed value in hmac-sha256 with prefix
 *
 * @api private
 */
function _sign_vals (key, vals, prefix, expiry, digestmod = 'sha256') {
  var exp = Math.round((new Date()).getTime() / 1000) + expiry;

  var val = vals + '|' + exp;

  var b64 = Buffer.from(val).toString('base64');
  var cookie = prefix + '|' + b64;

  var sig = crypto.createHmac(digestmod, key)
    .update(cookie)
    .digest('hex');

  return cookie + '|' + sig;
}

/**
 * @function parses a value
 *
 * @param {String} key Secret Key
 * @param {String} val Value to unpack
 * @param {String} prefix REQ|APP|AUTH Prefix
 * @param {String} wkey Web Key
 *
 * @return {String/Null} Returns a username on successful parse. Null if not
 *
 * @api private
 */
function _parse_vals (key, val, prefix, wid, digestmod = 'sha256') {
  var ts = Math.round((new Date()).getTime() / 1000);
  var parts = val.split('|');
  if (parts.length !== 3) {
    return null;
  }

  var u_prefix = parts[0];
  var u_b64 = parts[1];
  var u_sig = parts[2];

  var sig = crypto.createHmac(digestmod, key)
    .update(u_prefix + '|' + u_b64)
    .digest('hex');

  if (crypto.createHmac(digestmod, key).update(sig).digest('hex') !== crypto.createHmac(digestmod, key).update(u_sig).digest('hex')) {
    return null;
  }

  if (u_prefix !== prefix) {
    return null;
  }

  var cookie_parts = Buffer.from(u_b64, 'base64').toString('utf8').split('|');
  if (cookie_parts.length !== 3) {
    return null;
  }

  var u_wid = cookie_parts[0];
  var username = cookie_parts[1];
  var exp = cookie_parts[2];

  if (u_wid !== wid) {
    return null;
  }

  if (ts >= parseInt(exp)) {
    return null;
  }

  return username;
}

/**
 * @function sign's a login request to be passed onto Futurae
 *
 * @param {String} wid Web ID
 * @param {String} wkey Web Key
 * @param {String} skey Secret Key
 * @param {String} username Username
 *
 * @return {String} Futurae Signature
 *
 * @api public
 */
var sign_request = function (wid, wkey, skey, username) {
  if (!username || username.length < 1) {
    return ERR_USERNAME;
  }
  if (username.indexOf('|') !== -1) {
    return ERR_USERNAME;
  }
  if (!wid || wid.length !== WID_LEN) {
    return ERR_WID;
  }
  if (!wkey || wkey.length < WKEY_LEN) {
    return ERR_WKEY;
  }
  if (!skey || skey.length < SKEY_LEN) {
    return ERR_SKEY;
  }

  var vals = wid + '|' + username;

  var futurae_sig = _sign_vals(wkey, vals, REQUEST_PREFIX, FUTURAE_EXPIRY);
  var app_sig = _sign_vals(skey, vals, APP_PREFIX, APP_EXPIRY);

  return futurae_sig + ':' + app_sig;
};

/**
 * @function verifies a response from futurae Security
 *
 * @param {String} wid Web ID
 * @param {String} wkey Web Key
 * @param {String} skey Secret Key
 * @param {String} sig_response Signature Response from Futurae
 *
 * @param (String/Null} Returns a string containing the username if the response checks out. Returns null if it does not.
 *
 * @api public
 */
var verify_response = function (wid, wkey, skey, sig_response) {
  var parts = sig_response.split(':');
  if (parts.length !== 2) {
    return null;
  }

  var auth_sig = parts[0];
  var app_sig = parts[1];
  var auth_user = _parse_vals(wkey, auth_sig, AUTH_PREFIX, wid);
  var app_user = _parse_vals(skey, app_sig, APP_PREFIX, wid);

  if (auth_user !== app_user) {
    return null;
  }

  return auth_user;
};

/**
 * @function sign's a login request to be passed onto Futurae
 *
 * @param {String} wid Web ID
 * @param {String} skey Secret Key
 * @param {String} username Username
 *
 * @return {String} Futurae Signature
 *
 * @api public
 */
var sign_app_blob = function (wid, skey, username) {
  if (!username || username.length < 1) {
    throw new UsernameError('Username is invalid.');
  }
  if (username.indexOf('|') !== -1) {
    throw new UsernameError('Username is invalid.');
  }
  if (!wid || wid.length !== WID_LEN) {
    throw new WidError('Web ID is invalid.');
  }
  if (!skey || skey.length < SKEY_LEN) {
    throw new SkeyError('Secret key is invalid.');
  }

  var vals = wid + '|' + username;

  return _sign_vals(skey, vals, APP_PREFIX, APP_EXPIRY, 'sha512');
};

/**
 * @function verifies a response from futurae Security
 *
 * @param {String} wid Web ID
 * @param {String} skey Secret Key
 * @param {String} auth_user Returned username to verify
 * @param {String} app_sig The signed app_blob from the response response POST'ed
 *
 * @return {Boolean} Return True if auth_user validates against app_sig,
 * otherwise return False.
 *
 * @api public
 */
var verify_app_blob = function (wid, skey, auth_user, app_sig) {
  let app_user = '';
  try {
    app_user = _parse_vals(skey, app_sig, APP_PREFIX, wid, 'sha512');
  } catch (err) {
    return false;
  }
  return auth_user === app_user;
};

/**
 * @function Get and return a frame init txid from init api call
 *
 * @param {Client} client client object for web SDK 3 calls
 * @param {String} username username of the user
 * @param {String} wid Web ID
 * @param {String} skey Secret Key
 * @param {String} client_version current version
 * @param {Bool} enroll_only is user enrolling
 * @param {Function} callback called after response from Futurae
 *
 * @return {String} Return the response from the init api call
 *
 * @api public
*/
var initialize_auth = function (client, {username, wid, skey, client_version = 2.0, enroll_only = false}, callback) {
  var app_blob = sign_app_blob(wid, skey, username);
  var expiry = Math.floor((new Date()).getTime() / 1000) + INIT_EXPIRY;
  client_version = 'futurae_nodejs ' + client_version;
  client.init(username, app_blob, expiry, client_version, enroll_only, function(txid) {
    callback(txid);
  });
};

/**
 * @function Get validate, and return a frame authentication response or null.
 *
 * @param {Client} client client for Web SDK calls
 * @param {String} response_txid the response transaction id
 * @param {String} wid Web ID
 * @param {String} skey Secret Key
 * @param {Function} callback called after response from futurae security
 *
 * @return {String} Return response if the app_blob is verified, null otherwise
 *
 * @api public
*/
var verify_auth = function (client, {response_txid, wid, skey}, callback) {
  client.auth_response(response_txid, function(response) {
    if (!verify_app_blob(wid, skey, response.response.uname, response.response.app_blob)) {
      callback(null);
    } else {
      callback(response);
    }
  });
};

module.exports = {
  'sign_request': sign_request,
  'verify_response': verify_response,
  'sign_app_blob': sign_app_blob,
  'verify_app_blob': verify_app_blob,
  'initialize_auth': initialize_auth,
  'verify_auth': verify_auth,
  'ERR_USERNAME': ERR_USERNAME,
  'ERR_WID': ERR_WID,
  'ERR_SKEY': ERR_SKEY,
  'ERR_WKEY': ERR_WKEY,
  'UsernameError': UsernameError,
  'WidError': WidError,
  'SkeyError': SkeyError
};
