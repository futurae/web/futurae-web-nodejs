var http = require('http');
var fs = require('fs');
var url = require('url');
var qs = require('querystring');
var futurae_web = require('../index.js');

var wid = '';
var wkey = '';
var skey = '';
var host = '';
var lang = '';
var post_action = '';

var IFrame = function (host, sig) {
  return `<!DOCTYPE html>
  <html>
    <head>
      <title>Futurae Authentication Prompt</title>
      <meta name='viewport' content='width=device-width, initial-scale=1'>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <style>
        body {
          text-align: center;
        }
        iframe {
          width: 100%;
          min-width: 304px;
          max-width: 1280px;
          height: 800px;
          border: none;
        }
      </style>
    </head>
    <body>
      <h1>Futurae Authentication Prompt</h1>
      <script src="Futurae-Web-SDK-v1.js"></script>
      <iframe id="futurae_widget"
              title="Two-Factor Authentication"
              data-host=${host}
              data-sig-request=${sig}
              data-lang=${lang}
              allow="microphone" >
      </iframe>
    </body>
  </html>`;
};

var server = http.createServer(function (req, res) {
  var base_url = url.parse(req.url).pathname;
  var method = req.method;

  if (method === 'GET') {
    if (base_url === '/') {
      var query = url.parse(req.url, true).query;
      let {username} = query;

      if (username) {
        var sig = futurae_web.sign_request(wid, wkey, skey, username);
        var futurae_frame = IFrame(host, sig);

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(futurae_frame);

      } else {
        res.writeHead(404, {'Content-Type': 'text/html'});
        res.end(`Make sure you add a username:  http://localhost:8080/?username=xxxx,\
        and appropriate configuration variables (wid, wkey, skey, host) `);
      }
    } else {
      var filePath = '.' + req.url;
      fs.readFile(filePath, function(error, content) {
        if (error) {
            if(error.code == 'ENOENT'){
              res.writeHead(404, {'Content-Type': 'text/html'});
              res.end('Not found', 'utf-8');
            }
            else {
                res.writeHead(500);
                res.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                res.end();
            }
        }
        else {
            res.writeHead(200, { 'Content-Type': 'text/javascript' });
            res.end(content, 'utf-8');
        }
      });
    }
  } else if (method === 'POST') {
    if (base_url === post_action) {
      var request_body = '';

      req.on('data', function(data) {
        request_body += data.toString(); // convert Buffer to string
      });

      req.on('end', function () {
        var form_data = qs.parse(request_body);
        var sig_response = form_data.sig_response;

        var authenticated_username = futurae_web.verify_response(wid, wkey, skey, sig_response);

        if (authenticated_username) {
          res.end(`${authenticated_username}, You've been Futurae Authenticated !`);

        } else {
          res.status(401).end();
        }
      });
    }
  }
});

server.listen(8080, function () {
  console.log('Simple HTTP server listening on 8080');
});
