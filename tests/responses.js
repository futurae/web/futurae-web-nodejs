var crypto = require('crypto');

var invalid_response = function () {
  return 'AUTH|INVALID|SIG';
};

var bad_params_app = function (wid, skey, username) {
  var vals = wid + '|' + username + '|' + _future_expiry() + '| bad_param';
  cookie = _cookie('APP', vals);
  var sig = crypto.createHmac('sha256', skey)
              .update(cookie)
              .digest('hex');
  return cookie + '|' + sig;
};

var bad_params_response = function (wid, wkey, username) {
  var vals = wid + '|' + username + '|' + _future_expiry() + '| bad_param';
  cookie = _cookie('AUTH', vals);
  var sig = crypto.createHmac('sha256', wkey)
              .update(cookie)
              .digest('hex');
  return cookie + '|' + sig;
};

var future_response = function (wid, wkey, username) {
  var vals = wid + '|' + username + '|' + _future_expiry();
  cookie = _cookie('AUTH', vals);
  var sig = crypto.createHmac('sha256', wkey)
              .update(cookie)
              .digest('hex');
  return cookie + '|' + sig;
};

var expired_response = function (wid, wkey, username) {
  var vals = wid + '|' + username + '|' + _past_expiry();
  cookie = _cookie('AUTH', vals);
  var sig = crypto.createHmac('sha256', wkey)
              .update(cookie)
              .digest('hex');
  return cookie + '|' + sig;
};

var future_enroll_response = function (wid, wkey, username) {
  var vals = wid + '|' + username + '|' + _future_expiry();
  cookie = _cookie('ENROLL', vals);
  var sig = crypto.createHmac('sha256', wkey)
              .update(cookie)
              .digest('hex');
  return cookie + '|' + sig;

};

function _cookie (prefix, vals) {
  var b64 = Buffer.from(vals).toString('base64');
  return prefix + '|' + b64;
}

function _future_expiry () {
  return Math.floor((new Date()).getTime() / 1000) + 600;
}

function _past_expiry () {
  return Math.floor((new Date()).getTime() / 1000) - 600;
}

module.exports = {
  'invalid_response': invalid_response,
  'bad_params_app': bad_params_app,
  'bad_params_response': bad_params_response,
  'future_response': future_response,
  'expired_response': expired_response,
  'future_enroll_response': future_enroll_response
};
