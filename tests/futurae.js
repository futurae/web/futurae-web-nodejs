var Futurae = require('../index');
var responses = require('./responses');
var assert = require('assert');

var WID = 'FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX';
var WRONG_WID = 'FIXXXXXXXXXXXXXXXXXY';
var WKEY = 'futuraegeneratedsharedwebapplicationkey.';
var SKEY = 'useselfgeneratedhostapplicationsecretkey';
var WRONG_SKEY = 'useacustomerprovidedapplicationsecretkeY';
var USER = 'testuser';

describe('Signature Tests', function () {
  it('sign request with wid/wkey/skey and user', function (done) {
    var request_sig = Futurae.sign_request(WID, WKEY, SKEY, USER);
    assert.notEqual(request_sig, null, 'Invalid Request');
    done();
  });

  it('sign request without a user', function (done) {
    var request_sig = Futurae.sign_request(WID, WKEY, SKEY, '');
    assert.equal(request_sig, Futurae.ERR_USERNAME, 'Sign request user test failed');
    done();
  });

  it('sign request with invalid user', function (done) {
    var request_sig = Futurae.sign_request(WID, WKEY, SKEY, 'in|valid');
    assert.equal(request_sig, Futurae.ERR_USERNAME, 'Sign request user test failed');
    done();
  });

  it('sign request with an invalid wid', function (done) {
    var request_sig = Futurae.sign_request('invalid', WKEY, SKEY, USER);
    assert.equal(request_sig, Futurae.ERR_WID, 'Sign request wid test failed');
    done();
  });

  it('sign request with an invalid wkey', function (done) {
    var request_sig = Futurae.sign_request(WID, 'invalid', SKEY, USER);
    assert.equal(request_sig, Futurae.ERR_WKEY, 'Sign request wkey test failed');
    done();
  });

  it('sign request with an invalid skey', function (done) {
    var request_sig = Futurae.sign_request(WID, WKEY, 'invalid', USER);
    assert.equal(request_sig, Futurae.ERR_SKEY, 'Sign request skey test failed');
    done();
  });
});

var request_sig = Futurae.sign_request(WID, WKEY, SKEY, USER);
var parts = request_sig.split(':');
var valid_app_sig = parts[1];

request_sig = Futurae.sign_request(WID, WKEY, 'invalidinvalidinvalidinvalidinvalidinvalidinvalidinvalid', USER);
parts = request_sig.split(':');
var invalid_app_sig = parts[1];

var future_response = responses.future_response(WID, WKEY, USER);

describe('Verification Tests', function () {
  it('verify request', function (done) {
    var user = Futurae.verify_response(WID, WKEY, SKEY, responses.invalid_response() + ':' + valid_app_sig);
    assert.equal(user, null, 'Invalid response test failed');
    done();
  });
  it('expiry test', function (done) {
    var user = Futurae.verify_response(WID, WKEY, SKEY, responses.expired_response(WID, WKEY, USER) + ':' + valid_app_sig);
    assert.equal(user, null, 'Expired response test failed');
    done();
  });
  it('invalid app sig', function (done) {
    var user = Futurae.verify_response(WID, WKEY, SKEY, future_response + ':' + invalid_app_sig);
    assert.equal(user, null, 'Invalid app sig test failed');
    done();
  });
  it('verify response on valid signature', function (done) {
    var user = Futurae.verify_response(WID, WKEY, SKEY, future_response + ':' + valid_app_sig);
    assert.equal(user, USER, 'response verification failed on valid signature');
    done();
  });
  it('invalid response format', function (done) {
    var user = Futurae.verify_response(WID, WKEY, SKEY, responses.bad_params_response(WID, WKEY, USER) + ':' + valid_app_sig);
    assert.equal(user, null, 'Invalid response format test failed');
    done();
  });
  it('invalid app sig format', function (done) {
    var user = Futurae.verify_response(WID, WKEY, SKEY, future_response + ':' + responses.bad_params_app(WID, SKEY, USER));
    assert.equal(user, null, 'Invalid app sig format test failed');
    done();
  });
  it('wrong wid', function (done) {
    var user = Futurae.verify_response(WRONG_WID, WKEY, SKEY, future_response + ':' + valid_app_sig);
    assert.equal(user, null, 'Wrong WID test failed');
    done();
  });
});

describe('Signature App Blob Tests', function () {
  it('sign request with wid/skey and user', function (done) {
    var request_sig = Futurae.sign_app_blob(WID, SKEY, USER);
    assert.notEqual(request_sig, null);
    done();
  });

  it('sign request without a user', function (done) {
    assert.throws(function () {
      Futurae.sign_app_blob(WID, SKEY, '');
    }, Futurae.UsernameError);
    done();
  });

  it('sign request with invalid user', function (done) {
    assert.throws(function () {
      Futurae.sign_app_blob(WID, SKEY, 'in|valid');
    }, Futurae.UsernameError);
    done();
  });

  it('sign request with an invalid wid', function (done) {
    assert.throws(function () {
      Futurae.sign_app_blob('invalid', SKEY, USER);
    }, Futurae.WidError);
    done();
  });

  it('sign request with an invalid skey', function (done) {
    assert.throws(function () {
      Futurae.sign_app_blob(WID, 'invalid', USER);
    }, Futurae.SkeyError);
    done();
  });
});

describe('Verifcation App Blob Tests', function () {
  it('verify response on valid signature', function (done) {
    var request_sig = Futurae.sign_app_blob(WID, SKEY, USER);
    assert.notEqual(request_sig, null);
    done();
  });

  it('verify app blob invalid prefix', function (done) {
    var app_blob = Futurae.sign_app_blob(WID, SKEY, USER);
    var inv_app_blob = app_blob.replace('A', 'x');
    assert.equal(Futurae.verify_app_blob(WID, SKEY, USER, inv_app_blob), false, 'app blob verification failed on invalid app_blob');
    done();
  });

  it('verify app blob invalid user', function (done) {
    var app_blob = Futurae.sign_app_blob(WID, SKEY, USER);
    assert.equal(Futurae.verify_app_blob(WID, SKEY, 'invalid', app_blob), false, 'app blob verification failed on invalid user');
    done();
  });

  it('verify app blob invalid wid', function (done) {
    var app_blob = Futurae.sign_app_blob(WID, SKEY, USER);
    assert.equal(Futurae.verify_app_blob(WRONG_WID, SKEY, USER, app_blob), false, 'app blob verification failed on invalid wid');
    done();
  });

  it('verify app blob invalid skey', function (done) {
    var app_blob = Futurae.sign_app_blob(WID, SKEY, USER);
    assert.equal(Futurae.verify_app_blob(WID, WRONG_SKEY, USER, app_blob), false, 'app blob verification failed on invalid skey');
    done();
  });
});
